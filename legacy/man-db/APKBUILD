# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=man-db
pkgver=2.9.1
pkgrel=0
pkgdesc="The man command and related utilities for examining on-line help files"
url="https://www.nongnu.org/man-db/"
arch="all"
options="!check"  # requires //IGNORE in iconv
license="GPL-2.0+"
depends="groff gzip less"
makedepends_host="db-dev libpipeline-dev zlib-dev"
subpackages="$pkgname-doc $pkgname-lang"
triggers="man-db.trigger=/usr/share/man"
source="https://download.savannah.nongnu.org/releases/man-db/man-db-$pkgver.tar.xz
	man-db-2.8.5-iconv.patch
	"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--disable-setuid \
		--with-sections="1 1p 1x 2 2x 3 3p 3x 4 4x 5 5x 6 6x 7 7x 8 8x 9 0p tcl n l p o" \
		--enable-nls \
		--with-db=db
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	rm -r "${pkgdir}"/usr/lib/tmpfiles.d  # systemd
}

sha512sums="ae2d1e9f293795c63f5a9a1a765478a9a59cbe5fe6f759647be5057c1ae53f90baee8d5467921f3d0102300f2111a5026eeb25f78401bcb16ce45ad790634977  man-db-2.9.1.tar.xz
0a68260fc48488408dc11855858aa2569efa3aeefd765c425b8fc988e7fee3e1d42e19eb299e518afc9b2ae54c5d37911176127124a43d5041f1137af0457097  man-db-2.8.5-iconv.patch"
