# Maintainer: Kiyoshi Aman <adelie@aerdan.vulpine.house>
pkgname=patch
pkgver=2.7.6
pkgrel=4
pkgdesc="Utility to apply diffs to files"
url="https://www.gnu.org/software/patch/patch.html"
arch="all"
license="GPL-3.0+"
depends=""
checkdepends="autoconf automake bash ed"
makedepends=""
subpackages="$pkgname-doc"
source="https://ftp.gnu.org/gnu/$pkgname/$pkgname-$pkgver.tar.xz
	allow-missing.patch
	CVE-2018-1000156.patch
	CVE-2018-6951.patch
	CVE-2018-6952.patch
	CVE-2019-13636.patch
	CVE-2019-13638.patch
	"

# secfixes:
#   2.7.6-r2:
#     - CVE-2018-6951
#   2.7.6-r3:
#     - CVE-2018-1000156.patc
#   2.7.6-r4:
#     - CVE-2018-6952
#     - CVE-2019-13636
#     - CVE-2019-13638

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	make SHELL=bash check
}

package() {
	make prefix="$pkgdir"/usr \
		mandir="$pkgdir"/usr/share/man \
		-C "$builddir" install

	rm -f "$pkgdir"/usr/lib/charset.alias
	rmdir -p "$pkgdir"/usr/lib 2>/dev/null || true
}

sha512sums="fcca87bdb67a88685a8a25597f9e015f5e60197b9a269fa350ae35a7991ed8da553939b4bbc7f7d3cfd863c67142af403b04165633acbce4339056a905e87fbd  patch-2.7.6.tar.xz
317c922c3adcf347024a9ffd2374a1827b19cc1f275a90e195e070cbcf16fb47788b14ffd18365ae5e1f867ed650e6f9aed6acf287bfc427107f3ed8bcd2b3af  allow-missing.patch
93414b33413b493eaa15027dfbe39c00eb1c556acf9f30af4c0ca113303867c5e7ad441c2596a7f9d060b8b67735a2a1c8be5db3c779ea47302f616ef8530d5d  CVE-2018-1000156.patch
db51d0b791d38dd4f1b373621ee18620ae339b172f58a79420fdaa4a4b1b1d9df239cf61bbddc4e6a4896b28b8cffc7c99161eb5e2facaec8df86a1bf7755bc0  CVE-2018-6951.patch
99df964d826d400f87e9b82bf2600d8663c59bb8f9bf4aec082adc8cf6261744f37d416e15492d6e883202ade521d4436cb41c91f516085c3e6ce8e01a8956fb  CVE-2018-6952.patch
cecb80d8d48dfe66bc13c22a5ed0eb52157cc85a1b74d03d4a8ea1ebcfe5d59bae975aec34ac685adc71129dcdb794579fee0e221144412a7c1fa71c460f63c1  CVE-2019-13636.patch
d60f8c2364fca9b73aa73b5914cfd6571d11528d13fa7703ccfa93730cbdf8a6e4c9ca04cb7d02a40d33c38075890790b490052d5217e728b0948991da937980  CVE-2019-13638.patch"
