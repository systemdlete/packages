# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=procps
pkgver=3.3.16
pkgrel=0
pkgdesc="Utilities for monitoring your system and processes on your system"
url="https://gitlab.com/procps-ng/procps"
arch="all"
license="GPL-2.0+ LGPL-2.0+"
depends=""
checkdepends="dejagnu"
makedepends_build="autoconf automake libtool gettext-tiny"
makedepends_host="ncurses-dev utmps-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang libproc"
source="https://gitlab.com/procps-ng/procps/-/archive/v$pkgver/procps-v$pkgver.tar.bz2
	use-utmpx.patch
	"
builddir="$srcdir/$pkgname-v$pkgver"

prepare() {
	default_prepare
	printf "$pkgver" > .tarball-version
	./autogen.sh
}

build() {
	export LIBS="$LIBS -lutmps -lskarnet"
	export VERSION="$pkgver"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/ \
		--bindir=/bin \
		--sbindir=/sbin \
		--libdir=/lib \
		--mandir=/usr/share/man \
		--sysconfdir=/etc \
		--docdir=/usr/share/doc \
		--datarootdir=/usr/share \
		--disable-static \
		--disable-rpath \
		--with-ncurses \
		ac_cv_func_malloc_0_nonnull=yes \
		ac_cv_func_realloc_0_nonnull=yes
	make
}

check() {
	# can't use `make check`; 'po' dir has no check target
	make check-TESTS
}

package() {
	make DESTDIR="$pkgdir" ldconfig=true install="install -D" \
		install

	# These binaries are identical.
	ln -sf pgrep "$pkgdir"/bin/pkill

	install -d "$pkgdir"/usr/lib
	mv "$pkgdir"/include "$pkgdir"/usr/ \
		&& mv "$pkgdir"/lib/pkgconfig "$pkgdir"/usr/lib/
}

libproc() {
	pkgdesc="Library for monitoring system and processes"
	license="LGPL-2.1+"

	install -d "$subpkgdir"/
	mv "$pkgdir"/lib "$subpkgdir"/
}

sha512sums="633c6fe90e753f5c489de7f8728ca5fe4647663a3e58ab66f77229a5991f6e7a7d174ddec2d2dfcd3b65146dd04ac6ea9c98697457edf785cb516ec3ec120f0b  procps-v3.3.16.tar.bz2
6683b94f64848721ebce8254461fbd70a3efd048bf9579c5bbe298bd1ec00594c05640327f0e03475e583607f1f7405139b93fc9f06592d5593174cbf6d4119f  use-utmpx.patch"
