# Contributor: Steeve Chailloux <steeve@chaahk.com>
# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Contributor: Alyx Wolcott <alyx@leuhta.com>
# Maintainer: Alyx Wolcott <alyx@leuhta.com>
pkgname=ant
pkgver=1.10.8
pkgrel=0
pkgdesc="Java-based build tool"
url="http://ant.apache.org/"
arch="noarch"
options="!check"  # ExecStreamRedirectorTest locks on ppc64, SMTP tests fail
license="Apache-2.0"
depends="openjdk8"
makedepends=""
subpackages=""
source="https://www-eu.apache.org/dist/ant/source/apache-$pkgname-$pkgver-src.tar.gz
	ant.sh
	imageio_fix_java_test.patch
	imageio_fix_test.patch
	"
builddir="$srcdir/apache-$pkgname-$pkgver"

_anthome="/usr/lib/java/$pkgname"

# secfixes:
#   1.10.8-r0:
#     - CVE-2020-1945

build() {
	sh ./bootstrap.sh
	./bootstrap/bin/ant dist-lite
}

check() {
	./bootstrap/bin/ant test
}

package() {
	local destdir="$pkgdir/$_anthome"

	install -dm755 "$destdir"/bin
	rm dist/bin/*.bat dist/bin/*.cmd
	install -m755 dist/bin/* "$destdir"/bin

	install -dm755 "$pkgdir"/usr/bin
	ln -sf $_anthome/bin/ant "$pkgdir"/usr/bin/ant

	install -dm755 "$destdir"/lib
	install -m644 dist/lib/*.jar "$destdir"/lib

	# symlink to junit so it's on the javac build path for ant
	# matches behavior on ubuntu 9 and makes sense for compatibility
	ln -sf ../../junit.jar "$destdir"/lib/junit.jar

	# The license says the NOTICE file should be redistributed for
	# derivative works, so lets supply it.
	install -m644 -D NOTICE "$pkgdir"/usr/share/licenses/$pkgname/NOTICE

	install -m644 -D $srcdir/$pkgname.sh "$pkgdir"/etc/profile.d/$pkgname.sh
}

sha512sums="08312b38db21f0e9b32cbe45aebe42ff8abbc1feab0f7343c9c60ea751fb9ad3a92ecd36cdf89bc1cc77a381c945da6194fc611b26e385a915d8e2404cc168c6  apache-ant-1.10.8-src.tar.gz
955fe52b415d57716df1338eff6d667d80e06a67bffd53def7d63d96f1224157b724a92df647afe8b3243dea749e221aec1e86d5be01060408fa000ec1132374  ant.sh
9a698041a06ccc6db7765133fc6fabc6f2d0e70b96ff9de594bd836fc05ac33f489968a4fdd12fe044458eb3b99e905ca1e2a62a55acdbacebff9782c82a27de  imageio_fix_java_test.patch
1457c7415df1e80c8763b4db9f41cbdebfa58e17bcd263dae9ce31f9ee601650b51a893e4e0a324c974b60a6c2076fddfd78180baca0bcdbff65e9e88b30e063  imageio_fix_test.patch"
