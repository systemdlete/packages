# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=kdeplasma-addons
pkgver=5.18.4.1
pkgrel=0
pkgdesc="Extra applets and toys for KDE Plasma"
url="https://www.kde.org/"
arch="all"
license="GPL-2.0+ AND LGPL-2.1-only"
depends="qt5-qtquickcontrols qt5-qtquickcontrols2"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev
	qt5-qtx11extras-dev kactivities-dev kconfig-dev kconfigwidgets-dev
	kcmutils-dev kcoreaddons-dev kdoctools-dev ki18n-dev knewstuff-dev
	kross-dev krunner-dev kservice-dev kunitconversion-dev kholidays-dev
	plasma-framework-dev plasma-workspace-dev"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/plasma/${pkgver%.*}/kdeplasma-addons-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		-Bbuild \
		.
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make -C build DESTDIR="$pkgdir" install
}

sha512sums="1ec38e56b132e1d774973d2a8a758bfc88de01c96a11e95507bfebb4c5fcce35c6f9e446fabf5e9754678232d31ee95d8470340f7d5c861960ceca81b20b45ee  kdeplasma-addons-5.18.4.1.tar.xz"
