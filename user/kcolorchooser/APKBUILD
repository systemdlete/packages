# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kcolorchooser
pkgver=20.04.3
pkgrel=0
pkgdesc="Simple application to choose a colour from the screen"
url="https://www.kde.org/applications/graphics/kcolorchooser/"
arch="all"
license="MIT"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev ki18n-dev kxmlgui-dev"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kcolorchooser-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="21ceff866c4658be0672625e9f3143d47a1ca0afd08e82db4a2648e8369522c4746b762db1e36c9da462ab109d7c55184dcd59c99843b52bc9c6a2f8cd031909  kcolorchooser-20.04.3.tar.xz"
