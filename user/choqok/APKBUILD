# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=choqok
pkgver=1.7.0
pkgrel=0
pkgdesc="Microblogging client from KDE"
url="https://kde.org/applications/internet/org.kde.choqok"
arch="all"
license="GPL-3.0+"
depends=""
makedepends="qt5-qtbase-dev qt5-qtnetworkauth-dev cmake extra-cmake-modules
	kcmutils-dev kconfigwidgets-dev kcoreaddons-dev kdoctools-dev
	kemoticons-dev kglobalaccel-dev kguiaddons-dev ki18n-dev kio-dev
	knotifications-dev knotifyconfig-dev ktextwidgets-dev kwallet-dev
	kwidgetsaddons-dev kxmlgui-dev purpose-dev qca-dev sonnet-dev
	kdewebkit-dev kparts-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/choqok/1.7/src/choqok-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="6f5998078e555f7b4a40270ad0a4defa2d15bb8440b810320fc4cfc38d78cb34bb9b2f6a38eb33eb3217b51a78a7fbc7f79224cd0ddb0fcbf8c9035d8e600e96  choqok-1.7.0.tar.xz"
