# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=marble
pkgver=20.04.3
pkgrel=0
pkgdesc="Free, open-source map and virtual globe"
url="https://marble.kde.org/"
arch="all"
options="!check"  # Test suite requires package to be already installed.
license="LGPL-2.1-only AND GPL-2.0-only"
depends="shared-mime-info"
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev qt5-qtsvg-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qttools-dev zlib-dev
	qt5-qtserialport-dev krunner-dev kcoreaddons-dev kwallet-dev knewstuff-dev
	kio-dev kparts-dev kcrash-dev ki18n-dev phonon-dev plasma-framework-dev
	qt5-qtpositioning-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang $pkgname-libs"
source="https://download.kde.org/stable/release-service/$pkgver/src/marble-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="5c29b0a25164b50f197c826258d8de8b4e1a386d91fa7a6f67eb3da64197b2cefef31b0759c3bd19e9a5b0f0c639adc779d71a7ef1d7b51fca4e58cc3c8530cd  marble-20.04.3.tar.xz"
