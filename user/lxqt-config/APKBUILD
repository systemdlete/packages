# Contributor: Kiyoshi Aman <adelie@aerdan.vulpine.house>
# Maintainer: Kiyoshi Aman <adelie@aerdan.vulpine.house>
pkgname=lxqt-config
pkgver=0.15.0
_lxqt_build=0.7.0
pkgrel=0
pkgdesc="Collection of tools for configuring LXQt and the underlying system"
url="https://lxqt.github.io/"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.1+"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtsvg-dev qt5-qttools-dev
	liblxqt-dev>=${pkgver%.*}.0 lxqt-build-tools>=$_lxqt_build
	libxcursor-dev eudev-dev kwindowsystem-dev libkscreen-dev
	xf86-input-libinput-dev libxi-dev"
subpackages="$pkgname-doc"
source="https://github.com/lxqt/lxqt-config/releases/download/$pkgver/lxqt-config-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		${CMAKE_CROSSOPTS} -Bbuild
	make -j1 -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="68dc8ecc38033a7a93167fb0afbe45b3e14eb1132c3faf3395ebed1fc96b5836e8db003ecffde0417dfaf9e73b70fcc69a06523319f1f858cd2020eb61113a0d  lxqt-config-0.15.0.tar.xz"
