# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Contributor: Luis Ressel <aranea@aixah.de>
# Maintainer:
pkgname=nftables
pkgver=0.9.6
pkgrel=0
pkgdesc="Netfilter tables userspace tools"
url="https://netfilter.org/projects/nftables"
arch="all"
options="!check"  # No test suite.
license="GPL-2.0+ AND GPL-2.0"
depends=""
makedepends="asciidoctor bison flex gmp-dev libmnl-dev libnftnl-dev
	autoconf automake libtool"
subpackages="$pkgname-doc $pkgname-openrc"
source="https://netfilter.org/projects/nftables/files/$pkgname-$pkgver.tar.bz2
	asciidoctor.patch
	nftables.confd
	nftables.initd
	"

prepare() {
	default_prepare
	autoreconf -vif
}

build() {
	# TODO: Man pages are massively broken with asciidoctor
	A2X=asciidoctor bash ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--without-cli
	make
}

package() {
	make DESTDIR="$pkgdir" install

	install -Dm755 "$srcdir"/$pkgname.initd "$pkgdir"/etc/init.d/$pkgname
	install -Dm644 "$srcdir"/$pkgname.confd "$pkgdir"/etc/conf.d/$pkgname
}

sha512sums="ca6524ff1cb1e79d636afeb96f54e4699773e1cbda8e9a3ec5728f4d5b764c0df16b195cdcc0e304ae5643c8761b6b5a6685c737965a7415aec07aeb9f3dc5df  nftables-0.9.6.tar.bz2
f7b18945f0ab8be2a8725fa902cb2499de0a886076ae4cc337ebd845b3ae08f05a75b1680b428075d42558e7953014a227405e748741e6ebc3a7ac84bbf4beaa  asciidoctor.patch
4eb1adf003dfcaad65c91af6ca88d91b7904c471aefae67e7d3c2f8e053e1ac196d3437a45d1fed5a855b876a0f1fc58a724e381d2acf1164d9120cadee73eef  nftables.confd
58daafb012b7cd0248a7db6e10f6a667e683347aaea7eaa78cb88780272f334e00913cea3fd39a22a4a72acc27fabd101944b40916f4b534ddeb509bd0232017  nftables.initd"
