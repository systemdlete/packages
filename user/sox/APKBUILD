# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Maintainer: 
pkgname=sox
pkgver=14.4.2
pkgrel=2
pkgdesc="Convert between various audio formats"
url="http://sox.sourceforge.net/"
arch="all"
options="!check"  # No test suite.
license="GPL-2.0+ AND LGPL-2.1+ AND MIT AND BSD-1-Clause"
makedepends="alsa-lib-dev ffmpeg-dev file-dev flac-dev gsm-dev lame-dev
	libao-dev libid3tag-dev libmad-dev libogg-dev libsamplerate-dev
	libsndfile-dev libvorbis-dev opusfile-dev pulseaudio-dev
	autoconf automake libtool"
depends=""
subpackages="$pkgname-dev $pkgname-doc"
source="https://downloads.sourceforge.net/sourceforge/$pkgname/$pkgname-$pkgver.tar.gz
	CVE-2017-11332.patch
	CVE-2017-11358.patch
	CVE-2017-11359.patch
	CVE-2017-15370.patch
	CVE-2017-15371.patch
	CVE-2017-15372.patch
	CVE-2017-15642.patch
	CVE-2017-18189.patch
	CVE-2019-13590.patch
	CVE-2019-8354.patch
	CVE-2019-8355.patch
	CVE-2019-8356.patch
	CVE-2019-8357.patch
	disable-pipe-file-detection.patch
	"

# secfixes:
#   14.4.2-r2:
#     - CVE-2017-11332
#     - CVE-2017-11358
#     - CVE-2017-11359
#     - CVE-2017-15370
#     - CVE-2017-15371
#     - CVE-2017-15372
#     - CVE-2017-15642
#     - CVE-2017-18189
#     - CVE-2019-13590
#     - CVE-2019-8354
#     - CVE-2019-8355
#     - CVE-2019-8356
#     - CVE-2019-8357

prepare() {
	default_prepare
	autoreconf -vif
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--with-dyn-default \
		--with-distro="${DISTRO_NAME:-Adélie Linux}"
	make
}

package() {
	make DESTDIR="$pkgdir" install
	ln -sf play "$pkgdir"/usr/bin/rec
	ln -sf ../man1/sox.1.gz "$pkgdir"/usr/share/man/man7/soxeffect.7
	rm "$pkgdir"/usr/lib/sox/*.a
}

sha512sums="b5c6203f4f5577503a034fe5b3d6a033ee97fe4d171c533933e2b036118a43a14f97c9668433229708609ccf9ee16abdeca3fc7501aa0aafe06baacbba537eca  sox-14.4.2.tar.gz
b4bded0b15a2243fbb404a33fccf45ad5634d6c6e0f60b49b967592f00ff9bc3657ebcfa42b18a5af804e7f04a700773bece5951739b8206b2c68c46c9ec4c7f  CVE-2017-11332.patch
28ffd5eef149563a1cdfcd920a89a0e2247ddbf4ed10a76c4e34ee57e3a30ec3a98bacf53fe1675da1431dd40c30b13cae0b9a1e26153c1aad6144322b7d78b0  CVE-2017-11358.patch
2c95d85f94877bf1637d2c1297944a77a8854506cad35b1c0d632237133cd970da82cded817696b19acde25b0e570f4c86659cc362a910e4ea76a037e3e56214  CVE-2017-11359.patch
bcbfd0785751372cd959a7419d88af24bb041dd02d3d0cf2f0dab46b6f6b55f284c1d823d20e5a0eae15191f3ccb2eefa2026287fdfbecb064722b006970ee00  CVE-2017-15370.patch
b116887f52eb4b70de9dda5f14e581579c4c1755c39100d88c4b8645bf9e053cfe87de3346eb138edc45fd2c36f0e1755f91e09511d279fe6d4661099c578420  CVE-2017-15371.patch
f8a4d38cfad80a50b9c758b222d83f6b51d96f1491862680e1632eec2a5c2a7c6f968660307f0f403e0b7537f7da19a510945648bdef8f1302fd4683be869581  CVE-2017-15372.patch
259980ea6fe08a2481a478a4a21b11a7fc4390b1b53023009d85fb2185ee63c42d2762e024af20912e7277688fac98e4eaa66b4a4e79840517ff2481ad50327e  CVE-2017-15642.patch
de510114a9fbbbabe62149f3c22ebd1fae65ed68e6ed0b818f367bbee806c9e04be6db0c8e64f4985b7bd95dd0cc643e1475767fda4e405931f25104b4a2e39f  CVE-2017-18189.patch
eab27e22035bdbe00d0dc4117f98bf9c5dcad4513a27e0e8a83506b94fca8055bc6ce532d24306aa8434942bef111b3511daf260df56fafb7b4ac5ed2075e3f7  CVE-2019-13590.patch
61342fad71dbe7f0ff10a7327eeed901c0defd5aafaace4ac755032ccf687d875856490c30f2af050823fd6ff1a1c7f503ae26670225eab916ab59fa857a8cb3  CVE-2019-8354.patch
3f05ab71680a67c9e8a4b33c70cb19a623f0925a2620ab007dc8d4a82caf5b73b50e3e5d40e242d6f65420d444b91e11bee09e4398e8079ca4af60bd34097593  CVE-2019-8355.patch
6eca5096c658a61939902a70d218b5662b663df84173d09d5b23f497bdcb81c04cd94d8debed2818079c342cec80ec29ff33d572611826bdbc12a5d465a20241  CVE-2019-8356.patch
82fbbf62a7124248ce74cf0daab0cd224a3da80e62923db58b8be31c4f145abe0e653f6968d0f6b862e5554d080d0f85b0bc0bcdb6dea34c130aa4ee9106d915  CVE-2019-8357.patch
eb90574a7c174a32ac77aa09a2bb4ebbea407463517e55943e16efd8b7c52393c6b7a6b2778d696f708627271f4d2212221a85fc50d2500b32143139a37a957a  disable-pipe-file-detection.patch"
