# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kpat
pkgver=20.04.3
pkgrel=0
pkgdesc="Collection of card games for KDE"
url="https://games.kde.org/game.php?game=kpat"
arch="all"
options="!check"  # Requires running X11
license="GPL-2.0-only"
depends="libkdegames-carddecks"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev kconfig-dev
	kcompletion-dev kconfigwidgets-dev kcoreaddons-dev kcrash-dev ki18n-dev
	kdbusaddons-dev kdoctools-dev kguiaddons-dev kio-dev knewstuff-dev
	kwidgetsaddons-dev kxmlgui-dev libkdegames-dev shared-mime-info
	freecell-solver-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kpat-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="d178a1360b37acdbba9fa33de56929b51c19b81ff6ee01d3f8780fe507800d9c9c1155bd0ae4e2f8b6baff5410133020c113aa6ed3f86aee2238f448216ce7ad  kpat-20.04.3.tar.xz"
