# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=oxygen
pkgver=5.18.4.1
pkgrel=0
pkgdesc="'Oxygen' theme for KDE"
url="https://www.kde.org/"
arch="all"
license="MIT AND (GPL-2.0-only OR GPL-3.0-only) AND LGPL-2.1-only AND GPL-2.0+ AND (LGPL-2.1-only OR LGPL-3.0-only) AND LGPL-2.1+"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev libxcb-dev
	frameworkintegration-dev kcmutils-dev kcompletion-dev kconfig-dev
	kdecoration-dev kguiaddons-dev ki18n-dev kservice-dev
	kwidgetsaddons-dev kwindowsystem-dev"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/plasma/${pkgver%.*}/oxygen-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="b6804c0ff7b2234603c6e9d0e9957b8eb5b44b663aa427571a841b7ee421c7e95da2213afb4d956390c5a87828a530a13aecf7f86be63c0f56410259771c516d  oxygen-5.18.4.1.tar.xz"
