# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=mesa
pkgver=19.3.4
_llvmver=8
pkgrel=0
pkgdesc="Mesa DRI OpenGL library"
url="https://www.mesa3d.org"
arch="all"
options="!check"  # No test suite.
license="MIT AND X11 AND SGI-B-2.0 AND BSL-1.0 AND LGPL-2.1+"
depends=""
depends_dev="libdrm-dev libxcb-dev libxdamage-dev libxext-dev libxshmfence-dev
	xorgproto-dev"
makedepends="$depends_dev bison eudev-dev expat-dev flex libelf-dev
	libva-dev libvdpau-dev libx11-dev libxfixes-dev libxrandr-dev libxt-dev
	libxv-dev libxvmc-dev libxxf86vm-dev llvm$_llvmver-dev makedepend meson
	ninja python3 py3-libxml2 py3-mako zlib-dev"
subpackages="$pkgname-dev $pkgname-dri
	$pkgname-glapi $pkgname-egl $pkgname-gl $pkgname-gles
	$pkgname-xatracker $pkgname-osmesa $pkgname-gbm"
#	requires glslang: $pkgname-vulkan-overlay
source="https://mesa.freedesktop.org/archive/mesa-$pkgver.tar.xz
	amdgpu-pthread-header.patch
	big-endian-flipping.patch
	intel-vulkan.patch
	musl-fixes.patch
	musl-fix-includes.patch
	no-tls.patch
	time64.patch
	"

_dri_driverdir=/usr/lib/xorg/modules/dri
_dri_drivers="r200,nouveau"
_gallium_drivers="r300,r600,radeonsi,nouveau,freedreno,swrast,virgl"
_vulkan_drivers="amd"

case "$CARCH" in
x86* | pmmx)
	_dri_drivers="${_dri_drivers},i965"
	_gallium_drivers="${_gallium_drivers},i915,svga"
	_vulkan_drivers="${_vulkan_drivers},intel"
	;;
aarch64 | arm*)
	_gallium_drivers="${_gallium_drivers},vc4,freedreno,tegra,kmsro,v3d,lima,panfrost,etnaviv"
	case "$CARCH" in
	armhf) CFLAGS="$CFLAGS -mfpu=neon";;
	esac
	;;
ppc64)
	_arch_conf="-Dpower8=false"
	;;
esac

build() {
	meson \
		-Dprefix=/usr \
		-Ddri-drivers-path=$_dri_driverdir \
		-Dplatforms=x11,drm,surfaceless \
		-Ddri-drivers=$_dri_drivers \
		-Dgallium-drivers=$_gallium_drivers \
		-Dvulkan-drivers=$_vulkan_drivers \
		-Dosmesa=gallium \
		$_arch_conf \
		build

	ninja -C build
}

package() {
	DESTDIR="$pkgdir" ninja -C build install
}

egl() {
	replaces="mesa"
	pkgdesc="Mesa libEGL runtime libraries"
	install -d "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libEGL.so* \
		"$subpkgdir"/usr/lib/
}

gl() {
	replaces="mesa"
	pkgdesc="Mesa libGL runtime libraries"
	install -d "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libGL.so* \
		"$subpkgdir"/usr/lib/
}

glapi() {
	replaces="$pkgname-gles"
	pkgdesc="Mesa OpenGL API"
	install -d "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libglapi.so.* \
		"$subpkgdir"/usr/lib/
}

gles() {
	replaces="mesa"
	pkgdesc="Mesa libGLESv2 runtime libraries"
	install -d "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libGLES*.so* \
		"$subpkgdir"/usr/lib/
}

xatracker() {
	pkgdesc="Mesa XA state tracker for VMware"
	install -d "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libxatracker*.so.* \
		"$subpkgdir"/usr/lib/
}

osmesa() {
	pkgdesc="Mesa offscreen rendering libraries"
	install -d "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libOSMesa.so.* \
		"$subpkgdir"/usr/lib/
}

gbm() {
	pkgdesc="Mesa GBM library"
	replaces="mesa"
	install -d "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libgbm.so.* \
		"$subpkgdir"/usr/lib/
}

vulkan_overlay() {
	pkgdesc="Vulkan layer to display information about the current window"
	install -d "$subpkgdir"/usr/lib
	install -d "$subpkgdir"/usr/share/vulkan/explicit_layer.d
	mv "$pkgdir"/usr/lib/libVkLayer_MESA_overlay.so \
		"$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/share/vulkan/explicit_layer.d/VkLayer_MESA_overlay.json \
		"$subpkgdir"/usr/share/vulkan/explicit_layer.d/
}

dri() {
	pkgdesc="Mesa DRI drivers"
	replaces="mesa-dri-ati mesa-dri-freedreno mesa-dri-intel
	mesa-dri-nouveau mesa-dri-swrast mesa-dri-tegra mesa-dri-vc4
	mesa-dri-virtio mesa-dri-vmwgfx"
	provides="$replaces"
	install -d "$subpkgdir"/usr/lib/xorg/modules
	install -d "$subpkgdir"/usr/share/vulkan
	mv "$pkgdir"/usr/lib/dri "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/xorg/modules/dri "$subpkgdir"/usr/lib/xorg/modules/
	mv "$pkgdir"/usr/share/drirc.d "$subpkgdir"/usr/share/
	# XvMC drivers
	mv "$pkgdir"/usr/lib/libXvMC*.so* "$subpkgdir"/usr/lib/
	# support non-Vulkan arches
	mv "$pkgdir"/usr/lib/libvulkan*.so* "$subpkgdir"/usr/lib/ || true
	mv "$pkgdir"/usr/lib/vdpau "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/share/vulkan/icd.d "$subpkgdir"/usr/share/vulkan/
}

sha512sums="2bbb3dc8f1d839f11fe12cc959393cd69607fa6714b2166b80299e0559d2d3b0ac38ed4e15ac3e5f472264eb24536d1901d350f7409f3a7e00d6f4ccbb2312fb  mesa-19.3.4.tar.xz
245d0d64d858dfadeeb544f31f7d0bb6ecb746a7fd5ec99755d679ae1a1eef4198d66473fb24d333eb6786bb8657012771e8285d67f165dc61a031df801947aa  amdgpu-pthread-header.patch
3417e5c6d7ec564178e1d72431042b0a6ba659321f13a3dda81eda5fa0f2c8bc7c6972cb8266aea84ab05976ffb161659e9988c50ecc418e8bc1e1ce8f93a65f  big-endian-flipping.patch
ba954ea9aa49e5cdfec08f310f41abf09e01a2a889a09b6c32a154b750d3ebb2bfb5a9b7d244c06d26442688aeeb7f212f5f3c98c6db69f878098a49d476ff70  intel-vulkan.patch
9f7a050f09571a2b17098d495b82e2e85b293fb7285e7d6d7c3c48cd4220a1bdcc61a7321ba78dd14860939ecabe7e89b32d6110f3728f793273e1e26b78a553  musl-fixes.patch
c7d91a660a033df91fac9c557039efc8669f0c26b2d35997d50753938b70d1af0bd110dcab3f8236eafab7d4be5dd7cd128a3e057e67e7e6a38a73fd6a7ef62e  musl-fix-includes.patch
56e829b0570cf5215c191330a5162356b77eeb555eb8127e30da57bdff9d369c1bb3de3bd7ee36233d6d610c0cec81773879da05cc0b391e06ff4d7507bef8a8  no-tls.patch
0aad2fdc56ea8825533746ee4c2c803734f871e04da13ddd461b2de36716ba52e345e745a6861ffae62efacf6b64f629187ddda3c01e3d880ce20c0ac551dccb  time64.patch"
