# Contributor: Laurent Bercot <ska-adelie@skarnet.org>
# Maintainer: Laurent Bercot <ska-adelie@skarnet.org>
pkgname=s6-dns
pkgver=2.3.2.0
pkgrel=0
pkgdesc="skarnet.org's DNS client libraries and command-line DNS client utilities"
url="https://skarnet.org/software/s6-dns/"
arch="all"
options="!check"
license="ISC"
_skalibs_version=2.9
depends=""
makedepends="skalibs-dev>=$_skalibs_version skalibs-libs-dev>=$_skalibs_version"
subpackages="$pkgname-libs $pkgname-dev $pkgname-libs-dev:libsdev $pkgname-doc"
source="https://skarnet.org/software/$pkgname/$pkgname-$pkgver.tar.gz"

build() {
	./configure \
		--enable-shared \
		--enable-static \
		--disable-allstatic \
		--prefix=/usr \
		--libdir=/usr/lib \
		--libexecdir="/usr/lib/$pkgname" \
		--with-dynlib=/lib
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

libs() {
	pkgdesc="$pkgdesc (shared libraries)"
	depends="skalibs-libs>=$_skalibs_version"
	mkdir -p "$subpkgdir/usr/lib"
	mv "$pkgdir"/usr/lib/*.so.* "$subpkgdir/usr/lib/"
}

dev() {
	pkgdesc="$pkgdesc (development files)"
	depends="skalibs-dev>=$_skalibs_version"
	install_if="dev $pkgname=$pkgver-r$pkgrel"
	mkdir -p "$subpkgdir/usr/include" "$subpkgdir/usr/lib"
	mv "$pkgdir/usr/include" "$subpkgdir/usr/"
	mv "$pkgdir"/usr/lib/*.a "$subpkgdir/usr/lib/"
}

libsdev() {
	pkgdesc="$pkgdesc (development files for dynamic linking)"
	depends="$pkgname-dev"
	mkdir -p "$subpkgdir/usr/lib"
	mv "$pkgdir"/usr/lib/*.so "$subpkgdir/usr/lib/"
}

doc() {
	default_doc
	mkdir -p "$subpkgdir/usr/share/doc"
	cp -a "$builddir/doc" "$subpkgdir/usr/share/doc/$pkgname"
}

sha512sums="c4b833a944e6c99f52283d7c27c4a4937995b99afb1e9a9d601bb20efec7b23e05fb8500b1653c3a1c045811087acd0bc800cebfdd2fb60c6eba2fd85cfd580d  s6-dns-2.3.2.0.tar.gz"
