# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=ktexteditor
pkgver=5.72.0
pkgrel=0
pkgdesc="Reusable, programmable text editor widget"
url="https://www.kde.org/"
arch="all"
options="!check"  # Requires running X11 session.
license="LGPL-2.1+ AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends=""
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev qt5-qtxmlpatterns-dev
	karchive-dev kconfig-dev kguiaddons-dev ki18n-dev kio-dev kparts-dev
	syntax-highlighting-dev libgit2-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 doxygen graphviz
	qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/ktexteditor-$pkgver.tar.xz"

# secfixes:
#   5.52.0-r0:
#     - CVE-2018-10361

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="8dca328976699af3cb726f28a78caa11986056b2018a6c5e6d614e63ad0c247d69543aff72766cb000b7cf3fa30505889c95af7fde1b11fdd87bb21d69591521  ktexteditor-5.72.0.tar.xz"
