# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=sonnet
pkgver=5.72.0
pkgrel=0
pkgdesc="Framework for implementing portable spell check functionality"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules aspell-dev hunspell-dev
	qt5-qttools-dev doxygen graphviz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang $pkgname-aspell"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/sonnet-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	# Highlighter test requires X11
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E sonnet-test_highlighter
}

package() {
	make DESTDIR="$pkgdir" install
}

aspell() {
	pkgdesc="$pkdesc (aspell backend)"
	install_if="$pkgname=$pkgver-$pkgrel aspell"
	mkdir -p "$subpkgdir"/usr/lib/qt5/plugins/kf5/sonnet
	mv "$pkgdir"/usr/lib/qt5/plugins/kf5/sonnet/sonnet_aspell.so \
		"$subpkgdir"/usr/lib/qt5/plugins/kf5/sonnet/sonnet_aspell.so
}

sha512sums="8c5f552d9676d9be3cef0c69e986fbdd8b523d343e2a71e866a745ff555e5bb83829ab7a0ef7c1460104f91395ac912308c14c8c29da79e5d021d0401ce211d4  sonnet-5.72.0.tar.xz"
