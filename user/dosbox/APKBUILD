# Contributor: Horst Burkhardt <horst@adelielinux.org>
# Maintainer: Horst Burkhardt <horst@adelielinux.org>
pkgname=dosbox
pkgver=0.82.7
pkgrel=2
pkgdesc="Emulator for MS-DOS games"
url="https://github.com/joncampbell123/dosbox-x/"
arch="all !s390x"
options="!check" # dosbox does not ship tests
license="GPL-2.0-only"
depends=""
makedepends="ncurses-dev zlib-dev libxkbfile-dev libpng-dev libpcap-dev
	fluidsynth-dev sdl2-dev alsa-lib-dev autoconf automake libtool"
source="https://github.com/joncampbell123/dosbox-x/archive/dosbox-x-wip-20180513-1316.tar.gz
	porttalk-fix.patch
	asmfix.patch
	posix-headers.patch
	constness.patch
	gcc8-pmmx.patch
	"
builddir="$srcdir/dosbox-x-dosbox-x-wip-20180513-1316"

build() {
	_extra_conf=""

	case $CTARGET_ARCH in
	pmmx) _extra_conf="--disable-dynamic-x86";;
	esac

	./autogen.sh
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--enable-debug \
		--enable-core-inline \
		--enable-sdl2 \
		$_extra_conf
	make 
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="38b7423b695f3b0dfd26f8c8913f26afba73ea6235bb205ec3a5d7f08ef3c74a9b6ce2577f0ea8eb49724b548e3386332256d13f27469b165986702650638992  dosbox-x-wip-20180513-1316.tar.gz
daf0efea03d6295f5a20d8d197f7d0ba38e0608edcfe8be19fc6091b783885d523557674f0f7df83b88186b77794723106cf7a0e02125d2cc75ecfd7d51fa91d  porttalk-fix.patch
266ede57d21030f7752287f18660b47e9185a9aef9d022337dded9c9ce93e572691b41aca878f8a28c67bf5d217c7e84912336c21aacf594085dc31d99862ebf  asmfix.patch
462e84d5bac8fb2c1317b303d1bdda72a50e9eedfb5251450cb8d0f98f4297d6a483eb2c95624cbc8d25e917788032dd1e7d8b0d06d82dad6600f8bd6eec85f4  posix-headers.patch
775d8f209ea04a81fda5c3dcf8186b529248befb962019c66e60344a5e07c95462f9cc4acf0f82c44635fc86f1b4817030792a1900d36e6d911b6f782722f320  constness.patch
0e23ccef2c238583b1fbbf598da95f6e5f805edef37bd08d0d0c4a69e92f11b935b94dd3a8d889dc9587ec264b8540169b2858116f20d8467c345a179893090d  gcc8-pmmx.patch"
