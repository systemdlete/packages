# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=attica
pkgver=5.72.0
pkgrel=0
pkgdesc="Qt 5-based implementation of Open Collaboration Services"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1-only"
depends=""
makedepends="cmake extra-cmake-modules doxygen graphviz qt5-qtbase-dev
	qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/attica-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="b8730c6b3f0b268c07e2b6f9752ccf0b41953148bdb91087ddee5bbdfb496012e119486e66c48ecb9d4790a875baa8e0066e0c008396027f0ee440f74648c38b  attica-5.72.0.tar.xz"
