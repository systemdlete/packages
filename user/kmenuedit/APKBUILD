# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=kmenuedit
pkgver=5.18.4.1
pkgrel=0
pkgdesc="Menu editor for Plasma 5"
url="https://www.KDE.org/"
arch="all"
license="GPL-2.0+ AND GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kdbusaddons-dev ki18n-dev
	kiconthemes-dev kglobalaccel-dev kinit-dev kio-dev kitemviews-dev kxmlgui-dev
	sonnet-dev kdoctools-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/plasma/${pkgver%.*}/kmenuedit-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="6a4e45a8c97de0badea82d70ec1085b3b34c06fd11723dd024263e966bbbbae5cfc4c42dfe217f79e59d84723c418f7146f0dee55ace0fbb77645714731f4a49  kmenuedit-5.18.4.1.tar.xz"
