# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kcmutils
pkgver=5.72.0
pkgrel=0
pkgdesc="Framework for writing System Settings modules"
url="https://api.kde.org/frameworks/kcmutils/html/index.html"
arch="all"
options="!check"  # Test suite requires running X11 session.
license="LGPL-2.1-only AND LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev kitemviews-dev kservice-dev
	kconfigwidgets-dev kiconthemes-dev kdeclarative-dev kxmlgui-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 doxygen graphviz
	qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kcmutils-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="7709e077b93999df5f3abca570a6f75f6866e70ca666dae02220b12daf92f0dfe06fa1f2bd62333e63e45472e6a0094d59df7d8b8b4b69bc37d8d8c4396d34fd  kcmutils-5.72.0.tar.xz"
