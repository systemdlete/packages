# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=uwsgi
pkgver=2.0.19.1
pkgrel=0
pkgdesc="Web application server"
url="https://projects.unbit.it/uwsgi"
arch="all"
options="!check"  # Requires unpackaged `cppcheck`.
license="GPL-2.0-only"
depends=""
makedepends="libxml2-dev openssl-dev zlib-dev
	perl-dev php7-dev php7-embed python3-dev ruby-dev"
subpackages=""
# Keep psgi at the end for the CFLAGS hack.
_plugins="asyncio cgi corerouter http logfile php python rack psgi"
for _plugin in $_plugins; do
	subpackages="$subpackages $pkgname-$_plugin"
done
source="https://projects.unbit.it/downloads/uwsgi-$pkgver.tar.gz"

build() {
	echo 'plugin_dir = /usr/lib/uwsgi/plugins' >> buildconf/core.ini
	echo 'plugin_build_dir = .' >> buildconf/core.ini

	python3 uwsgiconfig.py --build core

	for _plugin in $_plugins; do
		[ $_plugin != "psgi" ] || export CFLAGS="-D_GNU_SOURCE -include /usr/include/setjmp.h"
		python3 uwsgiconfig.py --plugin plugins/$_plugin core
	done
}

check() {
	python3 uwsgiconfig.py --check
}

package() {
	install -D -m755 "$builddir"/uwsgi "$pkgdir"/usr/bin/uwsgi
}

_plugpack() {
	pkgdesc="$pkgdesc ($2 plugin)"
	depends=""

	install -D -m755 "$builddir"/$1_plugin.so \
		"$subpkgdir"/usr/lib/uwsgi/plugins/$1_plugin.so
}

asyncio() {
	_plugpack asyncio "Python asyncio"
}

cgi() {
	_plugpack cgi "CGI"
}

corerouter() {
	_plugpack corerouter "Core router"
}

http() {
	_plugpack http "HTTP server"
}

logfile() {
	_plugpack logfile "Log file"
}

php() {
	_plugpack php "PHP"
}

psgi() {
	_plugpack psgi "PSGI"
}

python() {
	_plugpack python "Python 3"
}

rack() {
	_plugpack rack "Ruby Rack"
}

sha512sums="e18359c5ea3606d5de7fb3a0074d777e1c4b41f51f4b1a21f1446f32253058d7924286d9ed8a159d4c6e6c0ea64df7fc3c70b2933c3433b9a9cce39737e702bf  uwsgi-2.0.19.1.tar.gz"
