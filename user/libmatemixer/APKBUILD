# Contributor: Kiyoshi Aman <adelie@aerdan.vulpine.house>
# Maintainer: Kiyoshi Aman <adelie@aerdan.vulpine.house>
pkgname=libmatemixer
pkgver=1.24.0
pkgrel=0
pkgdesc="Sound mixer library for the MATE desktop environment"
url="https://mate-desktop.org"
arch="all"
license="LGPL-2.0+"
depends=""
makedepends="alsa-lib-dev intltool pulseaudio-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang
	$pkgname-alsa $pkgname-pulse"
source="https://pub.mate-desktop.org/releases/${pkgver%.*}/libmatemixer-$pkgver.tar.xz"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

alsa() {
	pkgdesc="$pkgdesc (ALSA backend)"
	install_if="$pkgname=$pkgver-r$pkgrel alsa-lib"
	mkdir -p "$subpkgdir"/usr/lib/$pkgname
	mv "$pkgdir"/usr/lib/$pkgname/$pkgname-alsa.so "$subpkgdir"/usr/lib/$pkgname
}

pulse() {
	pkgdesc="$pkgdesc (PulseAudio backend)"
	install_if="$pkgname=$pkgver-r$pkgrel pulseaudio"
	mkdir -p "$subpkgdir"/usr/lib/$pkgname
	mv "$pkgdir"/usr/lib/$pkgname/$pkgname-pulse.so "$subpkgdir"/usr/lib/$pkgname
}

sha512sums="bf8c807bd45f3626bbf881eefa1e4638165daf61637bc63a704492d2a67eeec622e35e58c041649d33411294f96f79c99b3ea2406ae6fcf83294ec625cca9dbf  libmatemixer-1.24.0.tar.xz"
